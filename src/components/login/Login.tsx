import * as React from "react";
import './Login.css';
import {useState} from "react";
import logo from '../logo.png';

export interface Props {
    login: (username: string, password: string) => void;
}

const Login = ({login}: Props) => {
    const [username, setUsername] = useState<string>('');
    const [password, setPassword] = useState<string>('');

    const formSubmit = (e: React.FormEvent) => {
        e.preventDefault();
        login(username, password);
    };

    return (
        <div className="signin-container text-center">
            <form className="form-signin" onSubmit={formSubmit}>
                <img className="mb-4" src={logo} alt="" width="72" height="72"/>
                <h1 className="h3 mb-3 font-weight-normal">Time Tracker</h1>
                <label htmlFor="inputEmail" className="sr-only">Email address</label>
                <input type="email" id="inputEmail" className="form-control" placeholder="Email address" required
                       autoFocus onChange={({target}) => setUsername(target.value)}/>
                <label htmlFor="inputPassword" className="sr-only">Password</label>
                <input type="password" id="inputPassword" className="form-control" placeholder="Password" required
                       onChange={({target}) => setPassword(target.value)}/>
                <button className="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                <p className="mt-3 mb-3 text-muted">&copy; 2019</p>
            </form>
        </div>
    );
};

export default Login;
