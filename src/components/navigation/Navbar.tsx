import React from 'react';
import {Navbar as BsNavbar} from "react-bootstrap";
import {Nav} from 'react-bootstrap';
import {NavLink} from "react-router-dom";
import logo from '../logo.png';

const Navbar = () => {
    return (
        <BsNavbar bg="light" expand="lg">
            <BsNavbar.Brand as={NavLink} to="/"><img
                src={logo}
                width="30"
                height="30"
                className="d-inline-block align-top"
                alt="Producement logo"
            />{' TimeTracker'}</BsNavbar.Brand>
            <BsNavbar.Toggle aria-controls="basic-navbar-nav"/>
            <BsNavbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    <Nav.Link as={NavLink} to="/" exact={true}>Calendar</Nav.Link>
                    <Nav.Link as={NavLink} to="/reports">Reports</Nav.Link>
                </Nav>
            </BsNavbar.Collapse>
        </BsNavbar>
    );
};

export default Navbar;
