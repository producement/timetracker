import * as React from "react";
import {Col, Container, Form} from "react-bootstrap";
import {useState} from "react";
import ProjectGraph from "./graph/ProjectGraph";
import TaskGraph from "./graph/TaskGraph";
import moment, {Moment} from "moment";
import {Route, RouteComponentProps} from "react-router";
import DateTime from 'react-datetime';

import 'react-datetime/css/react-datetime.css';

const Reports = ({match}: RouteComponentProps) => {
    const [start, setStart] = useState<Moment>(moment().startOf('month'));
    const [end, setEnd] = useState<Moment>(moment().endOf('month'));

    const handleStart = (value: string | Moment) => {
        if (typeof value !== "string") {
            setStart(value);
        }
    };

    const handleEnd = (value: string | Moment) => {
        if (typeof value !== "string") {
            setEnd(value);
        }
    };

    return (
        <Container fluid={true}>
            <Form>
                <Form.Row>
                    <Col>
                        <DateTime closeOnSelect={true} value={start} onChange={handleStart}/>
                    </Col>
                    <Col>
                        <DateTime closeOnSelect={true} value={end} onChange={handleEnd}/>
                    </Col>
                </Form.Row>
            </Form>
            <Route path={`${match.path}/projects/:id`}
                   render={() => <TaskGraph start={start} end={end}/>}/>
            <Route exact path={match.path} render={() => <ProjectGraph start={start} end={end}/>}/>
        </Container>
    );
};

export default Reports;
