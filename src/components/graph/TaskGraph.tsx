import * as React from "react";
import {Col, Row} from "react-bootstrap";
import {useState} from "react";
import api from "../../api";
import {HorizontalBar} from "react-chartjs-2";
import * as chartjs from "chart.js";
import {useAsyncEffect} from 'use-async-effect';
import moment, {Moment} from "moment";
import Task from "../../api/Task";
import {RouteComponentProps, withRouter} from "react-router";
import Project from "../../api/Project";

interface RouteParams {
    id: string,
}

interface Props extends RouteComponentProps<RouteParams> {
    start: Moment,
    end: Moment,
}

const TaskGraph = ({start, end, match}: Props) => {
    const [data, setData] = useState<chartjs.ChartData | null>(null);
    const [project, setProject] = useState<Project | null>(null);

    const options = {
        scales: {
            xAxes: [{
                ticks: {
                    beginAtZero: true,
                }
            }]
        }
    };

    useAsyncEffect(async () => {
        const project = await api.getProject(match.params.id);
        setProject(project);
    }, undefined, [match.params.id]);

    useAsyncEffect(async () => {
        if (project != null) {
            const events = await api.getEvents(start, end);
            const projectEvents = events
                .filter((event) => event.task.project.id === match.params.id);

            const projectTasks = projectEvents.reduce((list, event) => {
                if (!list.some((t) => t.id === event.task.id)) {
                    list.push(event.task);
                }
                return list;
            }, new Array<Task>());

            const hoursByTask = (task: Task): number => {
                const taskEvents = projectEvents.filter((event) => event.task.id === task.id);
                return taskEvents.reduce((acc, event) => {
                    const duration = moment.duration(moment(event.end).diff(event.start));
                    const hours = duration.asHours();
                    return acc + hours;
                }, 0);
            };

            setData({
                labels: projectTasks.map((p) => p.name),
                datasets: [{
                    label: `Time logged by task in project ${project.name} between ${start.format('lll')} and ${end.format('lll')}`,
                    data: projectTasks.map(hoursByTask),
                }],
            });
        }
    }, undefined, [start, end, project]);

    return (
        data && <Row>
            <Col>
                <HorizontalBar data={data} options={options}/>
            </Col>
        </Row>
    );
};

export default withRouter(TaskGraph);
