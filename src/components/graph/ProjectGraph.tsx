import * as React from "react";
import {Col, Row} from "react-bootstrap";
import {useState} from "react";
import api from "../../api";
import {Bar} from "react-chartjs-2";
import * as chartjs from "chart.js";
import Project from "../../api/Project";
import moment, {Moment} from "moment";
import {Redirect} from "react-router";
import {useAsyncEffect} from "use-async-effect";

interface Props {
    start: Moment,
    end: Moment,
}

const ProjectGraph = ({start, end}: Props) => {
    const [data, setData] = useState<chartjs.ChartData | null>(null);
    const [project, setProject] = useState<Project | null>(null);
    const [projects, setProjects] = useState<Project[]>([]);

    const options = {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                }
            }]
        }
    };

    useAsyncEffect(async () => {
        const events = await api.getEvents(start, end);
        const projects = await api.getProjects();
        setProjects(projects);

        const hoursByProject = (project: Project): number => {
            const projectEvents = events.filter((event) => event.task.project.id === project.id);
            return projectEvents.reduce((acc, event) => {
                const duration = moment.duration(moment(event.end).diff(event.start));
                const hours = duration.asHours();
                return acc + hours;
            }, 0);
        };

        setData({
            labels: projects.map((p) => p.name),
            datasets: [{
                label: `Time logged by project between ${start.format('lll')} and ${end.format('lll')}`,
                data: projects.map(hoursByProject)
            }]
        })
    }, undefined, [start, end]);

    const choose = async ([chartElement]: any) => {
        const project = projects[chartElement._index];
        setProject(project);
    };

    if (project) {
        return (
            <Redirect to={`/reports/projects/${project.id}`}/>
        );
    }

    return (
        <Row>
            <Col>
                {data && <Bar data={data} options={options} getElementAtEvent={(e) => choose(e)}/>}
            </Col>
        </Row>
    );
};

export default ProjectGraph;
