import BigCalendar, {Event} from 'react-big-calendar';
import * as moment from 'moment';
import 'moment/locale/en-gb';
import * as React from "react";
import {useEffect, useState} from "react";
import 'react-big-calendar/lib/css/react-big-calendar.css';
import api from '../../api';
import CalendarEvent from "../../api/CalendarEvent";
import {Col, Container, Row} from "react-bootstrap";
import './Calendar.css';
import CalendarModal from "./CalendarModal";
import {confirmAlert} from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';

moment.locale('en-GB');

const localizer = BigCalendar.momentLocalizer(moment);

export interface TEvent extends Event {
    start: Date,
    end: Date,
    task: number | null,
}

const Calendar = () => {
    const [event, setEvent] = useState<TEvent | null>(null);
    const [events, setEvents] = useState<CalendarEvent[] | null>(null);

    useEffect(() => {
        if (events == null) {
            api.getEvents().then(setEvents);
        }
    }, [events]);

    const handleSelect = ({start, end}: any) => {
        setEvent({start, end, task: null});
    };

    const onModalFinish = (changed: boolean) => {
        setEvent(null);
        if (changed) {
            setEvents(null);
        }
    };

    function handleDoubleClick(event: CalendarEvent) {
        confirmAlert({
            title: 'Delete event',
            message: `Are you sure to delete ${event.title}?`,
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => {
                        event.destroy();
                        setEvents(null);
                    }
                },
                {
                    label: 'No',
                    onClick: () => {
                    }
                }
            ]
        });
    }

    return (
        <Container fluid={true}>
            <Row>
                <Col className="calendar-container">
                    <BigCalendar
                        selectable={true}
                        views={['month', 'week', 'work_week', 'day']}
                        events={events || []}
                        localizer={localizer}
                        startAccessor="start"
                        endAccessor="end"
                        scrollToTime={new Date(1970, 1, 1, 8)}
                        onSelectSlot={handleSelect}
                        defaultView={BigCalendar.Views.WORK_WEEK}
                        onDoubleClickEvent={handleDoubleClick}
                    />
                    <CalendarModal event={event} onFinish={onModalFinish}/>
                </Col>
            </Row>
        </Container>

    )
};

export default Calendar;
