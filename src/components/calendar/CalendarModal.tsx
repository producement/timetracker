import {Button, Col, Form, Modal, Row} from "react-bootstrap";
import * as React from "react";
import {useEffect, useState} from "react";
import Task from "../../api/Task";
import api from "../../api";
import Project from "../../api/Project";
import CalendarEvent from "../../api/CalendarEvent";
import {Typeahead} from "react-bootstrap-typeahead";
import moment from "moment";
import {TEvent} from "./Calendar";

interface Props {
    event: TEvent | null,
    onFinish: (changed: boolean) => void,
}

const CalendarModal = ({event, onFinish}: Props) => {

    let typeahead: React.RefObject<Typeahead<any>> = React.createRef();

    const [tasks, setTasks] = useState<Task[] | null>(null);
    const [task, setTask] = useState<Task[]>([]);
    const [project, setProject] = useState<number>(0);
    const [projects, setProjects] = useState<Project[] | null>(null);

    useEffect(() => {
        if (projects == null) {
            api.getProjects().then(setProjects);
        }
    }, [projects]);

    useEffect(() => {
        if (projects != null) {
            api.getTasks(projects[project]).then(setTasks);
        }
    }, [project, projects]);

    if (event == null) {
        return (<div/>);
    }

    const getCurrentTaskName = () => {
        const instance = typeahead.current as any;
        return instance.getInstance().getInput().value;
    };

    const getTask = async (): Promise<Task> => {
        const name = getCurrentTaskName();
        if (task.length !== 0) {
            return task[0];
        } else if (tasks) {
            const existing = tasks.find((t) => t.name === name);
            if (existing) {
                return existing;
            }
        }
        const newTask = new Task();
        if (projects) {
            newTask.project = projects[project];
        }
        newTask.name = name;
        return api.saveTask(newTask);
    };

    const saveEvent = async () => {
        const calendarEvent = new CalendarEvent();
        calendarEvent.start = event.start;
        calendarEvent.end = event.end;
        calendarEvent.task = await getTask();
        await api.save(calendarEvent);
        onFinish(true);
    };

    const selectProject = (e: any) => {
        setProject(e.target.value);
        setTask([]);
    };

    const TaskTypeahead = () => (<Typeahead
        ref={typeahead}
        id="taskTypeahead"
        labelKey="name"
        onChange={setTask}
        options={tasks || []}
        selected={task}
    />);

    const ProjectSelect = (props: any) => (
        <select onChange={selectProject} value={project} {...props}>
            {projects && projects.map((it, i) => (
                <option key={it.id} value={i}>{it.name}</option>))}
        </select>
    );

    return (
        <Modal show={event != null} onHide={() => onFinish(false)}>
            <Modal.Header closeButton>
                <Modal.Title>New Event</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <Form.Row>
                        <Form.Group as={Col}>
                            <Form.Label>Project</Form.Label>
                            <Form.Control as={ProjectSelect}/>
                        </Form.Group>
                    </Form.Row>
                    <Form.Row>
                        <Form.Group as={Col}>
                            <Form.Label>Task</Form.Label>
                            <Form.Control as={TaskTypeahead}/>
                        </Form.Group>
                    </Form.Row>
                    <Form.Group as={Row}>
                        <Form.Label column>
                            Start
                        </Form.Label>
                        <Col sm="10">
                            <Form.Control plaintext readOnly
                                          defaultValue={moment(event.start).format('lll')}/>
                        </Col>
                    </Form.Group>

                    <Form.Group as={Row}>
                        <Form.Label column>
                            End
                        </Form.Label>
                        <Col sm="10">
                            <Form.Control plaintext readOnly
                                          defaultValue={moment(event.end).format('lll')}/>
                        </Col>
                    </Form.Group>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={() => onFinish(false)}>
                    Close
                </Button>
                <Button variant="primary" onClick={saveEvent}>
                    Save Event
                </Button>
            </Modal.Footer>
        </Modal>
    );
};

export default CalendarModal;
