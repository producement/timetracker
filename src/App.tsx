import React, {useState} from 'react';
import Parse from 'parse';
import Calendar from "./components/calendar/Calendar";
import Login from "./components/login/Login";
import {HashRouter, Route} from "react-router-dom";
import Reports from "./components/Reports";
import Navbar from "./components/navigation/Navbar";
import {Container} from 'react-bootstrap';

const App = () => {
    const [user, setUser] = useState<Parse.User | undefined>(Parse.User.current());

    const login = async (username: string, password: string) => {
        const user = await Parse.User.logIn(username, password);
        setUser(user);
    };

    if (user == null) {
        return <Login login={login}/>
    }

    return (
        // Using HashRouter because the app is deployed to GitLab Pages
        <HashRouter>
            <React.Fragment>
                <Navbar/>
                <Container fluid={true} className="mt-4 mb-3">
                    <Route path="/" exact component={Calendar}/>
                    <Route path="/reports" component={Reports}/>
                </Container>
            </React.Fragment>
        </HashRouter>
    );
};

export default App;
