import Parse from 'parse';
import CalendarEvent from "./CalendarEvent";
import Project from "./Project";
import Task from "./Task";
import moment, {Moment} from "moment";

Parse.initialize("TimeTracker");

Parse.serverURL = 'https://producement-parse.herokuapp.com/parse';

class Api {

    public async getEvents(start: Moment = moment(0), end: Moment | null = null): Promise<CalendarEvent[]> {
        const query = new Parse.Query(CalendarEvent);
        query.greaterThan('start', start.toDate());
        if (end) {
            query.lessThan('end', end.toDate());
        }
        query.equalTo('user', Parse.User.current());
        query.include(['task.project']);
        query.limit(1000);
        return query.find().catch(this.handleParseError);
    }

    public getProjects(): Promise<Project[]> {
        return new Parse.Query(Project).find().catch(this.handleParseError);
    }

    public getProject(id: string): Promise<Project> {
        return new Parse.Query(Project).get(id).catch(this.handleParseError)
    }

    public getTasks(project: Project): Promise<Task[]> {
        const query = new Parse.Query(Task);
        query.equalTo('project', project);
        query.limit(1000);
        return query.find().catch(this.handleParseError);
    }

    public saveTask(task: Task): Promise<Task> {
        return task.save().catch(this.handleParseError);
    }

    public save(event: CalendarEvent): Promise<CalendarEvent> {
        event.user = Parse.User.current();
        return event.save().catch(this.handleParseError);
    }

    private handleParseError(err: any): never {
        if (err.code === 209) {
            Parse.User.logOut().finally(() => {
                window.location.reload();
            });
        }
        throw err;
    }

}

export default Api;
