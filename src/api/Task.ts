import Parse from 'parse';
import Project from "./Project";

class Task extends Parse.Object {
    constructor() {
        super('Task');
    }

    get name(): string {
        return this.get('name');
    }

    set name(name: string) {
        this.set('name', name);
    }

    get project(): Project {
        return this.get('project');
    }

    set project(project: Project) {
        this.set('project', project);
    }

}

Parse.Object.registerSubclass('Task', Task);

export default Task;
