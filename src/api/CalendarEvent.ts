import Parse, {User} from 'parse';
import Task from "./Task";

class CalendarEvent extends Parse.Object {
    constructor() {
        super('CalendarEvent');
    }

    get title(): String {
        const taskName = this.get('task').name;
        const projectName = this.get('task').get('project').name;
        return `${projectName} - ${taskName}`;
    }

    get start(): Date {
        return this.get('start');
    }

    set start(start: Date) {
        this.set('start', start);
    }

    get end(): Date {
        return this.get('end');
    }

    set end(end: Date) {
        this.set('end', end);
    }

    get allDay(): boolean {
        return this.get('allDay');
    }

    set allDay(allDay: boolean) {
        this.set('allDay', allDay);
    }

    get task(): Task {
        return this.get('task')
    }

    set task(task: Task) {
        this.set('task', task);
    }


    set user(user: User | undefined) {
        this.set('user', user);
    }
}

Parse.Object.registerSubclass('CalendarEvent', CalendarEvent);

export default CalendarEvent;
