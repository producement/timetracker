import Parse from 'parse';

class Project extends Parse.Object {
    constructor() {
        super('Project');
    }

    get name(): string {
        return this.get('name');
    }

}

Parse.Object.registerSubclass('Project', Project);

export default Project;
