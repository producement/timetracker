import Parse from 'parse';

class Client extends Parse.Object {
    constructor() {
        super('Client');
    }

    get name(): string {
        return this.get('name');
    }
}

Parse.Object.registerSubclass('Client', Client);

export default Client;
